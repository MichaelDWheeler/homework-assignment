(function(){
    'use strict';

    var express = require('express');
    var app = express();

    app.use(express.static('dist'));

    app.listen(process.env.PORT || 3000);
    console.log('Port 3000 is listening');
    
})();
