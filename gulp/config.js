(function(){
    'use strict';

    module.exports = {
        gulp: {
            path: {
                dist: 'dist',
                src: 'src',
                style: 'stylesheets'
            }
        }
    };

})();
