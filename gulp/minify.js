(function(){
    'use strict';

    var gulp = require('gulp');
    var path = require('./config');
    var $ = require('gulp-load-plugins')({
        pattern: ['gulp-*', 'del']
    });

    gulp.task('clean', function(done){
        return $.del([path.gulp.path.dist + '*/'], done);
    });

    gulp.task('css', function(){
        return gulp.src([path.gulp.path.style + '/*.css'])
        .pipe($.minifyCss())
        .pipe(gulp.dest(path.gulp.path.dist));
    });

    gulp.task('html', function(){
        return gulp.src([path.gulp.path.src + '/*.html'])
        .pipe($.minifyHtml())
        .pipe(gulp.dest(path.gulp.path.dist));
    });

    gulp.task('script', function(){
        return gulp.src([path.gulp.path.src + '/*.js'])
        .pipe($.concat('all.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest(path.gulp.path.dist));
    });

    gulp.task('buildApp', ['html', 'css', 'script']);

    gulp.task('build', ['clean'], function(){
        gulp.start('buildApp');
    });

    gulp.task('watch', function(){
        gulp.watch(path.gulp.path.src + '/**/*.html', ['html']);
        gulp.watch(path.gulp.path.style + '/**/*.css', ['css']);
        gulp.watch(path.gulp.path.src + '/**/*.js', ['script']);
    });

})();
