(function(){
    'use strict';

    var gulp = require('gulp');
    var nodemon = require('gulp-nodemon');

    gulp.task('nodemon', ['watch'], function(){
        return nodemon({
            script: 'app.js'
        });
    });

})();
