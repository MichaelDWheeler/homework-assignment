(function(){
    'use strict';

    var gulp = require('gulp');
    var path = require('./gulp/config');
    require('require-dir')('./gulp');

    gulp.task('default', ['build'], function(){
        gulp.start('nodemon');
    });

})();
