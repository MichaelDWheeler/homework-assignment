/*jshint loopfunc: true */
(function(){
    'use strict';

    // reusable variables kept out of namespace
    var element = (function(){
        var appTitleEl = document.getElementById('app-title');
        var blackCircleEl = document.getElementsByClassName('black-circle');
        var calcVariablesObj = {};
        var calcButtonEl = document.getElementById('calculate-btn');
        var coinEl = document.getElementsByClassName('coin');
        var coinValueEl = document.getElementsByClassName('coin-value');
        var blackCircleNumber = document.getElementsByClassName('coins-required');
        var editValueEl = document.getElementsByClassName('edit-value');
        var inputFieldEl = document.getElementById('number-input');
        var titleContainerEl = document.getElementById('title-container');

        return {
            appTitle: function(){
                return(appTitleEl);
            },
            blackCircle: function(){
                return(blackCircleEl);
            },
            calcButton: function(){
                return(calcButtonEl);
            },
            calcVariables: function(){
                return(calcVariablesObj);
            },
            coin: function(){
                return(coinEl);
            },
            coinDimensions: function(){
                return(Math.round(this.deviceWidth() * 0.3125));
            },
            circleDimensions: function(){
                return(Math.round(this.deviceWidth() * 0.078125));
            },
            coinValue: function(){
                return(coinValueEl);
            },
            blackCircleNum: function(){
                return(blackCircleNumber);
            },
            deviceWidth: function(){
                var userDeviceWidth = document.getElementById('calculator').clientWidth;
                return(userDeviceWidth);
            },
            editValue: function(){
                return(editValueEl);
            },
            inputField: function(){
                return(inputFieldEl);
            },
            titleContainer: function(){
                return(titleContainerEl);
            }
        };
    })();

    var alignCoins = function(){
        var appWidth = document.getElementById('calculator').clientWidth;
        var leftMargin = (appWidth - (element.coin()[0].clientWidth * 2)) / 3;
        for(var i = 0; i < 4; i++){
            element.coin()[i].style.marginLeft = leftMargin + "px";
        }
    };

    var getLargestHeight = function(element){
        var largestHeight = 0;
        for(var i = 0, length = element.length; i < length; i++){
            if(element[i].clientHeight > largestHeight){
                largestHeight = element[i].clientHeight;
            }
        }
        return largestHeight;
    };

    var alignNumbers = function(){
        var editValueWidth = 20;
        var coinValueHeight = element.coinValue()[0].clientHeight;
        var blackCircleNumHeight = getLargestHeight(element.blackCircleNum());
        var editValueMargin = Math.round((element.coinDimensions()/2) - (editValueWidth/2));
        var cvMarginTop = Math.round((element.coinDimensions()/2) - (coinValueHeight/2));
        var bcnMarginTop = Math.round((element.circleDimensions()/2) - (blackCircleNumHeight/2));
        for(var i = 0; i < 4; i++){
            element.coinValue()[i].style.cssText = "margin-top: " + cvMarginTop + "px";
            element.blackCircleNum()[i].style.cssText = "margin-top: " + bcnMarginTop + "px";
            element.editValue()[i].style.marginLeft = editValueMargin + "px";
        }
    };

    var mobileCalculateBtn = function(deviceHeight){
        var btnLength = Math.round(element.deviceWidth() * 0.75);
        var btnTopMargin = Math.round(deviceHeight * 0.05);
        element.calcButton().style.cssText = "width:" + btnLength + "px; margin-top:" +  btnTopMargin + "px";
    };

    var mobileCoins = function(callback){
        for(var i = 0; i < 4; i++){
            element.coin()[i].style.cssText = "height: " + element.coinDimensions() + "px; width: " + element.coinDimensions() + "px;";
        }
        callback();
        alignCoins();

    };

    var mobileFonts = function(){
        var body = document.getElementsByTagName('body')[0];
        var fontSize = Math.round(element.deviceWidth() * 0.05);
        body.style.cssText = "font-size: " + fontSize + "px;";
    };

    var mobileInput = function(deviceHeight){
        var inputLength = Math.round(element.deviceWidth() * 0.75);
        var inputTopMargin = Math.round(deviceHeight * 0.0833);
        element.inputField().style.cssText = "width: " + inputLength + "px; margin-top: " + inputTopMargin + "px;";
    };

    var mobileTitle = function(deviceHeight){
        var titleMargin = Math.round(deviceHeight * 0.075);
        var sizeFont = Math.round(element.deviceWidth() * 0.05);
        element.titleContainer().style.cssText = "margin-top: " + titleMargin +  'px;';
        element.appTitle().style.cssText = "font-size: " + sizeFont + "px;";
    };

    var designForMobile = function(){
        var deviceHeight = window.innerHeight && document.documentElement.clientHeight ?
        Math.min(window.innerHeight, document.documentElement.clientHeight) :
        window.innerHeight ||
        document.documentElement.clientHeight ||
        document.getElementsByTagName('body')[0].clientHeight;
        mobileFonts();
        mobileCoins(alignNumbers);
        mobileTitle(deviceHeight);
        mobileInput(deviceHeight);
        mobileCalculateBtn(deviceHeight);
    };

    var addEvent = function(object, type, callback) {
        if (object == null || typeof(object) == 'undefined') return;
        if (object.addEventListener) {
            object.addEventListener(type, callback, false);
        } else if (object.attachEvent) {
            object.attachEvent("on" + type, callback);
        } else {
            object["on"+type] = callback;
        }
    };

    var revertToDesktop = function(){
        var body = document.getElementsByTagName('body')[0];
        for(var i = 0; i < 4; i++){
            element.blackCircle()[i].style.cssText = "display:none;float:right;height:25px;width:25px;border-radius:100%;text-align:center;";
            element.coin()[i].style.cssText = "display: inline-block; margin-left: 40px; margin-top: 40px; width: 100px; height: 100px; border-radius: 100%; text-align: center;";
            element.coinValue()[i].style.cssText = "display: block;margin-top: 42px;";
        }
        element.calcButton().style.cssText = "display: block;margin: 30px auto 0 auto;height: 45px;width: 240px;text-align: center;";
        body.style.cssText = "margin: 0 !important;font-size: 16px";
        element.inputField().style.cssText = "display: block;margin: 50px auto 0 auto;padding-right: 20px;height: 45px;width: 240px;text-align: right;";
        element.titleContainer().style.cssText = "margin-top: 0";
    };

    var checkForMobile = function(){
        if(document.body.clientWidth <= 567){
            designForMobile();
        }else{
            revertToDesktop();
        }
    };

    addEvent(window, "resize", function(event) {
        checkForMobile();
    });

    var compareNumbers = function(a, b){
        return a - b;
    };

    var findCoinOrder = function(array){
        var sortedArray = array.slice();
        sortedArray = sortedArray.sort(compareNumbers);
        var indexOrder = [];
        for(var i = 0; i < 4; i ++){
            var number = sortedArray[i];
            indexOrder.unshift(array.indexOf(number));
        }
        return(indexOrder);
    };

    var coinValuesArray = function(value){
        var valueArray = [];
        for(var i = 0; i < 4; i++){
            var thisCoinsValue = parseInt(element.coinValue()[i].textContent);
            valueArray.push(thisCoinsValue);
        }
        return valueArray;
    };

    var showCoinsRequired = function(){
        var length = element.blackCircleNum().length;
        for(var i = 0;  i < length; i++){
            if(element.blackCircleNum()[i].innerHTML != "0"){
                element.blackCircle()[i].style.cssText = "display:block; height: " + element.circleDimensions() + "px; width: " + element.circleDimensions() + "px;";
                alignNumbers(element.circleDimensions());
            }else{
                element.blackCircle()[i].style.cssText = "display:none;float:right;height:25px;width:25px;border-radius:100%;text-align:center;";
            }
        }
    };

    var determineDenominations = function(value){
        var valueArray = coinValuesArray(value);
        var orderArray = findCoinOrder(valueArray);
        for(var i = 0, length = orderArray.length; i < length; i++){
            var totalCoins = Math.floor(value / valueArray[orderArray[i]]);
            value -= totalCoins * valueArray[orderArray[i]];
            element.blackCircleNum()[orderArray[i]].textContent = totalCoins;
        }
        showCoinsRequired();
    };

    var checkForNumber = function(value){
        if(isNaN(value) || value == null || value === ""){
            return true;
        }else{
            return false;
        }
    };

    var checkInputValue = function(){
        if(checkForNumber(element.inputField().value)){
            element.inputField().value = "";
            element.inputField().placeholder = "Please enter a number";
        }else{
            var value = Math.round(element.inputField().value);
            element.inputField().placeholder = element.inputField().value;
            determineDenominations(value);
        }
    };

    var checkForPennyCoin = function(){
        var displayedValue = document.getElementsByClassName('displayed-value');
        for(var i = 0; i < 4; i++){
            if(displayedValue[i].textContent === "1"){
                return true;
            }
        }
        return false;
    };

    var checkCoinValues = function(displayedValue, originalNumber, newValue){
        if(checkForNumber(newValue.value)){
            newValue.value = element.calcVariables().originalValue;
            newValue.focus();
        }else{
            displayedValue.textContent = newValue.value;
            if(checkForPennyCoin()){
                displayedValue.style.display = "block";
                newValue.style.display = "none";
            }else{
                alert("A penny must always be present");
                newValue.value = element.calcVariables().originalValue;
                displayedValue.textContent = newValue.value;
                displayedValue.style.display = "block";
                newValue.style.display = "none";
            }
        }
    };

    var hideNumberCircles = function(){
        for(var i = 0; i < 4; i++){
            element.blackCircle()[i].style.display = "none";
        }
    };

    var enterNumberFocus = function(){
        element.inputField().addEventListener('focus', function(){
            hideNumberCircles();
            element.inputField().value = "";
            element.inputField().placeholder = "";
        });
    }();

    var editCoinValue = function(){
        for(var i = 0; i < 4; i++){
            element.coinValue()[i].addEventListener('click', function(e){
                hideNumberCircles();
                element.inputField().value = "";
                element.inputField().placeholder = "";
                var parent = e.target.parentNode,
                editValue = parent.getElementsByClassName('edit-value')[0],
                displayedValue = parent.getElementsByClassName('displayed-value')[0];
                if(editValue.value !== ""){
                    element.calcVariables().originalValue = editValue.value;
                }
                editValue.value = "";
                editValue.style.display = "inline";
                displayedValue.style.display = "none";
                editValue.focus();
            });
        }
    }();

    var exitCoinEdit = function(){
        for(var i = 0; i < 4; i++){
            element.editValue()[i].addEventListener('blur', function(e){
                var parent = e.target.parentNode;
                var editValue = parent.getElementsByClassName('edit-value')[0];
                var displayedValue = parent.getElementsByClassName('displayed-value')[0];
                var originalNumber = { oldValue : editValue.value};
                checkCoinValues(displayedValue, originalNumber, editValue);
            }, true);
        }
    }();

    var hasDuplicates = function(array){
        for (var i = 0; i < array.length; i++){
            for (var j = 0; j < array.length; j++){
                if (i != j){
                    if (array[i] == array[j]){
                        return true;
                    }
                }
            }
        }
        return false;
    };

    var checkForDuplicates = function(){
        var valueArray = [];
        for(var i = 0; i < 4; i++){
            valueArray.push(element.coinValue()[i].textContent);
        }
        if(hasDuplicates(valueArray)){
            return true;
        }else{
            return false;
        }
    };

    var calculateDenomination = function(){
        element.calcButton().addEventListener('click', function(){
            if(checkForDuplicates()){
                alert("Each coin must contain a unique value");
            }else{
                checkInputValue();
            }
        });
    }();

    checkForMobile();
})();
